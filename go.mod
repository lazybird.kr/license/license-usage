module license-usage

go 1.13

require (
	github.com/DataDog/zstd v1.4.4 // indirect
	github.com/Shopify/sarama v1.25.0 // indirect
	github.com/apache/thrift v0.13.0 // indirect
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.5.0 // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang/protobuf v1.3.2
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
	github.com/lightstep/lightstep-tracer-go v0.18.1
	github.com/oklog/oklog v0.3.2
	github.com/oklog/run v1.1.0 // indirect
	github.com/opentracing-contrib/go-observer v0.0.0-20170622124052-a52f23424492 // indirect
	github.com/opentracing/basictracer-go v1.0.0 // indirect
	github.com/opentracing/opentracing-go v1.1.0
	github.com/openzipkin-contrib/zipkin-go-opentracing v0.3.5 // indirect
	github.com/openzipkin/zipkin-go-opentracing v0.3.5
	github.com/prometheus/client_golang v1.3.0
	github.com/prometheus/common v0.7.0
	go.mongodb.org/mongo-driver v1.2.1
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980
	google.golang.org/grpc v1.26.0
	lazybird.com/license-common v0.0.0
	sourcegraph.com/sourcegraph/appdash v0.0.0-20190731080439-ebfcffb1b5c0
)

replace lazybird.com/license-common => ../license-common
