package main

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"google.golang.org/grpc"
	"license-usage/usage/pkg/grpc/pb"
	"license-usage/usage/pkg/service"
	"os"
	"runtime"
	"strings"
)

var conn *grpc.ClientConn

func main() {
	grpcAddr := service.GetConfigServerUsageGrpc()
	var opts []grpc.DialOption
	var err error
	opts = append(opts, grpc.WithInsecure())
	conn, err = grpc.Dial(grpcAddr, opts...)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Dial to %v error", grpcAddr)
		return
	}

	var services []string
	services = append(services, "CreateKeyUsage")
	services = append(services, "UpdateKeyUsage")
	services = append(services, "ReadKeyUsage")
	services = append(services, "ReadKeyUsageWithMac")

	showUsage(services)

	var caseService string
	for {
		fmt.Fprintf(os.Stdout, "->")
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		text := scanner.Text()
		text = strings.TrimLeft(text, " ")

		var tokens []string
		if len(text) > 0 {
			tokens = strings.Split(text, " ")
			caseService = tokens[0]
		}

		switch caseService {
		case "CreateKeyUsage":
			CreateKeyUsage(tokens)
		case "UpdateKeyUsage":
			UpdateKeyUsage(tokens)
		case "ReadKeyUsage":
			ReadKeyUsage(tokens)
		case "ReadKeyUsageWithMac":
			ReadKeyUsageWithMac(tokens)
		}
	}
}

func ReadKeyUsageWithMac(tokens []string) {
	if len(tokens) < 2 {
		fmt.Fprintf(os.Stdout, "%s: <service> <mac_address>\n", GetFunctionName())
		return
	}

	var mac []string
	for i := 1; i < len(tokens); i++ {
		mac = append(mac, tokens[i])
		i++
	}

	message := &pb.ReadKeyUsageWithMacRequest{
		MacAddress: mac,
	}

	c := pb.NewUsageClient(conn)
	reply, err := c.ReadKeyUsageWithMac(
		context.Background(),
		message,
	)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return
	} else {
		fmt.Fprintf(os.Stdout, "< %v Reply: \n", GetFunctionName())
		jsonLog(reply)
	}

	return
}

func ReadKeyUsage(tokens []string) {
	if len(tokens) < 2 {
		fmt.Fprintf(os.Stdout, "%s: <service> <key>\n", GetFunctionName())
		return
	}

	message := &pb.ReadKeyUsageInfoRequest{
		Key: tokens[1],
	}

	c := pb.NewUsageClient(conn)
	reply, err := c.ReadKeyUsageInfo(
		context.Background(),
		message)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return
	} else {
		fmt.Fprintf(os.Stdout, "< %v Reply: \n", GetFunctionName())
		jsonLog(reply)
	}
}

func UpdateKeyUsage(tokens []string) {
	if len(tokens) < 3 {
		fmt.Fprintf(os.Stdout, "%s: <service> <key> <mac_address>\n", GetFunctionName())
	}

	message := &pb.UpdateKeyUsageInfoRequest{
		Key:        tokens[1],
		MacAddress: tokens[2],
	}

	c := pb.NewUsageClient(conn)
	reply, err := c.UpdateKeyUsageInfo(
		context.Background(),
		message)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return
	} else {
		fmt.Fprintf(os.Stdout, "< %v Reply: \n", GetFunctionName())
		jsonLog(reply)
	}
}

func CreateKeyUsage(tokens []string) {
	if len(tokens) < 2 {
		fmt.Fprintf(os.Stdout, "%s: <service> <key> \n", GetFunctionName())
	}

	message := &pb.CreateKeyUsageInfoRequest{
		Key: tokens[1],
	}

	c := pb.NewUsageClient(conn)
	reply, err := c.CreateKeyUsageInfo(
		context.Background(),
		message)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return
	} else {
		fmt.Fprintf(os.Stdout, "< %v Reply: \n", GetFunctionName())
		jsonLog(reply)
	}
}

func showUsage(servics []string) {
	fmt.Fprintf(os.Stdout, "\n")
	for i, s := range servics {
		fmt.Fprintf(os.Stdout, "%d. %s\n", i+1, s)
	}
	fmt.Fprintf(os.Stdout, "\n")
}

func GetFunctionName() string {
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])

	tokens := strings.Split(f.Name(), ".")

	return tokens[len(tokens)-1]
}

func jsonLog(message interface{}) {
	j, err := json.MarshalIndent(message, "", " ")
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
	} else {
		fmt.Fprintf(os.Stdout, "%v\n", string(j))
	}
	fmt.Fprintf(os.Stdout, "\n")
}
