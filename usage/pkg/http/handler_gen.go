// THIS FILE IS AUTO GENERATED BY GK-CLI DO NOT EDIT!!
package http

import (
	http "github.com/go-kit/kit/transport/http"
	mux "github.com/gorilla/mux"
	endpoint "license-usage/usage/pkg/endpoint"
	http1 "net/http"
)

// NewHTTPHandler returns a handler that makes a set of endpoints available on
// predefined paths.
func NewHTTPHandler(endpoints endpoint.Endpoints, options map[string][]http.ServerOption) http1.Handler {
	m := mux.NewRouter()
	makeKeepAliveUsageHandler(m, endpoints, options["KeepAliveUsage"])
	makeCreateKeyUsageInfoHandler(m, endpoints, options["CreateKeyUsageInfo"])
	makeUpdateKeyUsageInfoHandler(m, endpoints, options["UpdateKeyUsageInfo"])
	makeReadKeyUsageInfoHandler(m, endpoints, options["ReadKeyUsageInfo"])
	makeReadKeyUsageWithMacHandler(m, endpoints, options["ReadKeyUsageWithMac"])
	makeReadKeyUsageBulkHandler(m, endpoints, options["ReadKeyUsageBulk"])
	makeExpireUsageBulkHandler(m, endpoints, options["ExpireUsageBulk"])
	return m
}
