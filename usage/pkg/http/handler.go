package http

import (
	"context"
	"encoding/json"
	"errors"
	endpoint "license-usage/usage/pkg/endpoint"
	http1 "net/http"

	http "github.com/go-kit/kit/transport/http"
	handlers "github.com/gorilla/handlers"
	mux "github.com/gorilla/mux"
)

// makeCreateKeyUsageInfoHandler creates the handler logic
func makeCreateKeyUsageInfoHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/create-key-usage-info").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.CreateKeyUsageInfoEndpoint, decodeCreateKeyUsageInfoRequest, encodeCreateKeyUsageInfoResponse, options...)))
}

// decodeCreateKeyUsageInfoRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeCreateKeyUsageInfoRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.CreateKeyUsageInfoRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeCreateKeyUsageInfoResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeCreateKeyUsageInfoResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}
func ErrorEncoder(_ context.Context, err error, w http1.ResponseWriter) {
	w.WriteHeader(err2code(err))
	json.NewEncoder(w).Encode(errorWrapper{Error: err.Error()})
}
func ErrorDecoder(r *http1.Response) error {
	var w errorWrapper
	if err := json.NewDecoder(r.Body).Decode(&w); err != nil {
		return err
	}
	return errors.New(w.Error)
}

// This is used to set the http status, see an example here :
// https://github.com/go-kit/kit/blob/master/examples/addsvc/pkg/addtransport/http.go#L133
func err2code(err error) int {
	return http1.StatusInternalServerError
}

type errorWrapper struct {
	Error string `json:"error"`
}

// makeUpdateKeyUsageInfoHandler creates the handler logic
func makeUpdateKeyUsageInfoHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/update-key-usage-info").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.UpdateKeyUsageInfoEndpoint, decodeUpdateKeyUsageInfoRequest, encodeUpdateKeyUsageInfoResponse, options...)))
}

// decodeUpdateKeyUsageInfoRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeUpdateKeyUsageInfoRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.UpdateKeyUsageInfoRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeUpdateKeyUsageInfoResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeUpdateKeyUsageInfoResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeReadKeyUsageInfoHandler creates the handler logic
func makeReadKeyUsageInfoHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/read-key-usage-info").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.ReadKeyUsageInfoEndpoint, decodeReadKeyUsageInfoRequest, encodeReadKeyUsageInfoResponse, options...)))
}

// decodeReadKeyUsageInfoRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeReadKeyUsageInfoRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.ReadKeyUsageInfoRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeReadKeyUsageInfoResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeReadKeyUsageInfoResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeKeepAliveUsageHandler creates the handler logic
func makeKeepAliveUsageHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/keep-alive-usage").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.KeepAliveUsageEndpoint, decodeKeepAliveUsageRequest, encodeKeepAliveUsageResponse, options...)))
}

// decodeKeepAliveUsageRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeKeepAliveUsageRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.KeepAliveUsageRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeKeepAliveUsageResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeKeepAliveUsageResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeReadKeyUsageWithMacHandler creates the handler logic
func makeReadKeyUsageWithMacHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/read-key-usage-with-mac").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.ReadKeyUsageWithMacEndpoint, decodeReadKeyUsageWithMacRequest, encodeReadKeyUsageWithMacResponse, options...)))
}

// decodeReadKeyUsageWithMacRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeReadKeyUsageWithMacRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.ReadKeyUsageWithMacRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeReadKeyUsageWithMacResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeReadKeyUsageWithMacResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeReadKeyUsageBulkHandler creates the handler logic
func makeReadKeyUsageBulkHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/read-key-usage-bulk").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.ReadKeyUsageBulkEndpoint, decodeReadKeyUsageBulkRequest, encodeReadKeyUsageBulkResponse, options...)))
}

// decodeReadKeyUsageBulkRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeReadKeyUsageBulkRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.ReadKeyUsageBulkRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeReadKeyUsageBulkResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeReadKeyUsageBulkResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeExpireUsageBulkHandler creates the handler logic
func makeExpireUsageBulkHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/expire-usage-bulk").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.ExpireUsageBulkEndpoint, decodeExpireUsageBulkRequest, encodeExpireUsageBulkResponse, options...)))
}

// decodeExpireUsageBulkRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeExpireUsageBulkRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.ExpireUsageBulkRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeExpireUsageBulkResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeExpireUsageBulkResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}
