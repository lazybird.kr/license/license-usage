package service

import (
	"github.com/prometheus/common/log"
	"go.mongodb.org/mongo-driver/bson"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"os"
	"time"
)

type KeyUsageObject struct {
	Id         primitive.ObjectID `bson:"_id,omitempty"`
	Key        string             `bson:"key,omitempty"`
	MacAddress []string           `bson:"mac_address,omitempty"`
	Users      []string           `bson:"users,omitempty"`
	Time       TimeLogObject      `bson:"time,omitempty"`
	Status     string             `bson:"status,omitempty"` // standby, activate, expired
}

func (d *KeyUsageObject) Create() (id string, err error) {
	collection := GetCollection()

	d.Time.Initialize()

	name, err := collection.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys:    bsonx.Doc{{"key", bsonx.Int32(1)}},
			Options: options.Index().SetUnique(true),
		})
	fmt.Fprintf(os.Stdout, "%s is unique field", name)

	result, err := collection.InsertOne(context.TODO(), d)
	if err != nil {
		err = fmt.Errorf("insert: %v error", err)
		return
	}
	d.Id = result.InsertedID.(primitive.ObjectID)
	id = d.Id.Hex()

	return
}

func (d *KeyUsageObject) UpdateKeyUsage() (cnt int64, err error) {
	collection := GetCollection()

	addBson := bson.M{}
	if len(d.MacAddress) > 0 {
		addBson["mac_address"] = bson.M{"$each":d.MacAddress}
	}
	if len(d.Users) > 0 {
		addBson["users"] = bson.M{"$each": d.Users}
	}

	setBson := bson.M{}
	setBson["status"] = "activate"

	d.Time.Update()
	d.Time.Activate()
	setBson["time.update_time"] = d.Time.UpdateTime
	setBson["time.active_time"] = d.Time.ActivationTime
	onInsertBson := bson.M{}
	onInsertBson["time.create_time"] = time.Now().UTC()
	//setBson["time.active_time"] = time.Now().UTC()

	filter := bson.M{"key": d.Key}
	update := bson.M{
		"$addToSet": addBson,
		"$set": setBson,
		"$setOnInsert": onInsertBson,
	}

	opts := options.Update().SetUpsert(true)
	result, err := collection.UpdateOne(context.Background(), filter, update, opts)
	if err != nil {
		fmt.Errorf("%v\n", err)
		return
	}

	d.Time.Update()

	cnt = result.ModifiedCount

	return
}

func (d *KeyUsageObject) ReadKeyUsage() (obj KeyUsageObject, err error) {
	collection := GetCollection()

	readBson := bson.M{}
	readBson["key"] = d.Key

	err = collection.FindOne(context.Background(), readBson).Decode(&obj)
	if err != nil {
		fmt.Errorf("%v - key(%v)\n", err, d.Key)
		return
	}

	return
}

func (d *KeyUsageObject) ReadKeyWithMac() (result []*KeyUsageObject, err error) {
	collection := GetCollection()

	readBson := bson.M{}
	readBson["mac_address"] = bson.M{ "$in": d.MacAddress }

	cursor, err := collection.Find(context.Background(), readBson)
	if err != nil {
		err = fmt.Errorf("%v\n", err)
		return
	}

	for cursor.Next(context.TODO()) {
		var document KeyUsageObject
		err := cursor.Decode(&document)
		if err != nil {
			log.Fatal(err)
		}
		result = append(result, &document)
	}

	if err := cursor.Err(); err != nil {
		err = fmt.Errorf("%v\n", err)
	}

	cursor.Close(context.Background())

	return
}

func (d *KeyUsageObject) ReadUsagesBulkWithKey(keyList []string) (objs []*KeyUsageObject, err error) {
	collection := GetCollection()

	readBson := bson.M{}
	readBson["key"] = bson.M{"$in": keyList}

	cursor, err := collection.Find(context.Background(), readBson)
	if err != nil {
		err = fmt.Errorf("%v\n", err)
		return
	}
	defer cursor.Close(context.TODO())

	for cursor.Next(context.TODO()) {
		var result KeyUsageObject
		err := cursor.Decode(&result)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
		}
		objs = append(objs, &result)
	}

	return
}

func (d *KeyUsageObject) UpdateUsagesToExpired(keyList []primitive.ObjectID) (cnt int64, err error) {
	collection := GetCollection()

	filter := bson.M {}
	filter["_id"] = bson.M{ "$in": keyList}
	updateBson := bson.M{
		"$set": bson.M {
			"status": "expired",
		},
	}

	result, err := collection.UpdateMany(context.Background(), filter, updateBson)
	if err != nil {
		return
	}

	cnt = result.ModifiedCount

	return
}