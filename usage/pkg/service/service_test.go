package service

import (
	"context"
	"license-usage/usage/pkg/grpc/pb"

	"os"
	"testing"
	"github.com/go-kit/kit/log"
)

func makeService() (svc UsageService){
	logger := log.NewLogfmtLogger(os.Stderr)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = log.With(logger, "caller", log.DefaultCaller)
	logM := LoggingMiddleware(logger)

	middleWare := []Middleware { logM}
	svc = New(middleWare)

	return
}


func TestBasicUsageService_ReadKeyUsageBulk(t *testing.T) {
	keyList := []string { "5e4269f9193df31ae078ae93", "5e49f6f8e310c65178450102" }
	svc := makeService()

	req := pb.ReadKeyUsageBulkRequest{
		KeyList: keyList,
	}
	reply, err := svc.ReadKeyUsageBulk(context.TODO(), &req)
	if err != nil {
		t.Errorf("ReadKeyUsageBulk error - %v\n", err)
		return
	}else {
		t.Logf("ReadKeyUsageBulk: %v\n", reply)
	}
}
