package service

import (
	"context"
	"license-usage/usage/pkg/grpc/pb"

	log "github.com/go-kit/kit/log"
)

type Middleware func(UsageService) UsageService

type loggingMiddleware struct {
	logger log.Logger
	next   UsageService
}

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next UsageService) UsageService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) CreateKeyUsageInfo(ctx context.Context, req *pb.CreateKeyUsageInfoRequest) (res *pb.CreateKeyUsageInfoReply, err error) {
	defer func() {
		l.logger.Log("method", "CreateKeyUsageInfo", "req", req, "res", res, "err", err)
	}()
	return l.next.CreateKeyUsageInfo(ctx, req)
}

func (l loggingMiddleware) UpdateKeyUsageInfo(ctx context.Context, req *pb.UpdateKeyUsageInfoRequest) (res *pb.UpdateKeyUsageInfoReply, err error) {
	defer func() {
		l.logger.Log("method", "UpdateKeyUsageInfo", "req", req, "res", res, "err", err)
	}()
	return l.next.UpdateKeyUsageInfo(ctx, req)
}

func (l loggingMiddleware) ReadKeyUsageInfo(ctx context.Context, req *pb.ReadKeyUsageInfoRequest) (res *pb.ReadKeyUsageInfoReply, err error) {
	defer func() {
		l.logger.Log("method", "ReadKeyUsageInfo", "req", req, "res", res, "err", err)
	}()
	return l.next.ReadKeyUsageInfo(ctx, req)
}

func (l loggingMiddleware) KeepAliveUsage(ctx context.Context, req *pb.KeepAliveUsageRequest) (res *pb.KeepAliveUsageReply, err error) {
	defer func() {
		l.logger.Log("method", "KeepAliveUsage", "req", req, "res", res, "err", err)
	}()
	return l.next.KeepAliveUsage(ctx, req)
}

func (l loggingMiddleware) ReadKeyUsageWithMac(ctx context.Context, req *pb.ReadKeyUsageWithMacRequest) (res *pb.ReadKeyUsageWithMacReply, err error) {
	defer func() {
		l.logger.Log("method", "ReadKeyUsageWithMac", "req", req, "res", res, "err", err)
	}()
	return l.next.ReadKeyUsageWithMac(ctx, req)
}

func (l loggingMiddleware) ReadKeyUsageBulk(ctx context.Context, req *pb.ReadKeyUsageBulkRequest) (res *pb.ReadKeyUsageBulkReply, err error) {
	defer func() {
		l.logger.Log("method", "ReadKeyUsageBulk", "req", req, "res", res, "err", err)
	}()
	return l.next.ReadKeyUsageBulk(ctx, req)
}

func (l loggingMiddleware) ExpireUsageBulk(ctx context.Context, req *pb.ExpireUsageBulkRequest) (res *pb.ExpireUsageBulkReply, err error) {
	defer func() {
		l.logger.Log("method", "ExpireUsageBulk", "req", req, "res", res, "err", err)
	}()
	return l.next.ExpireUsageBulk(ctx, req)
}
