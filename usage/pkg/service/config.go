package service

import (
	"lazybird.com/license-common/config"
)

var DBConfig config.DBConfig
var ServerHostConfig config.ServerConfig

func init() {
	if err := config.LoadConfig(); err != nil {
		panic(err)
	}
	_ = config.GetConfig()

	DBConfig = config.GetDBConfig()
	ServerHostConfig = config.GetServerHostConfig()
}

func GetConfigServerUsageGrpc() string {
	return ServerHostConfig.Usage.GrpcHosts
}

func GetConfigServerUsageHttp() string {
	return ServerHostConfig.Usage.HttpHosts
}