package service

import "time"

type TimeLogObject struct {
	CreateTime     time.Time `bson:"create_time,omitempty"`
	UpdateTime     time.Time `bson:"update_time,omitempty"`
	ActivationTime time.Time `bson:"active_time,omitempty"`
}

func (t *TimeLogObject) Initialize() {
	t.CreateTime = time.Now().UTC()
	t.UpdateTime = t.CreateTime
}

func (t *TimeLogObject) Update() {
	t.UpdateTime = time.Now().UTC()
}

func (t *TimeLogObject) Activate() {
	t.ActivationTime = time.Now().UTC()
}
