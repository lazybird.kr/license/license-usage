package service

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"license-usage/usage/pkg/grpc/pb"
	"os"

	"lazybird.com/license-common/config"
)

// UsageService describes the service.
type UsageService interface {
	// Add your methods here
	// e.x: Foo(ctx context.Context,s string)(rs string, err error)
	KeepAliveUsage(ctx context.Context, req *pb.KeepAliveUsageRequest) (res *pb.KeepAliveUsageReply, err error)
	CreateKeyUsageInfo(ctx context.Context, req *pb.CreateKeyUsageInfoRequest) (res *pb.CreateKeyUsageInfoReply, err error)
	UpdateKeyUsageInfo(ctx context.Context, req *pb.UpdateKeyUsageInfoRequest) (res *pb.UpdateKeyUsageInfoReply, err error)
	ReadKeyUsageInfo(ctx context.Context, req *pb.ReadKeyUsageInfoRequest) (res *pb.ReadKeyUsageInfoReply, err error)
	ReadKeyUsageWithMac(ctx context.Context, req *pb.ReadKeyUsageWithMacRequest) (res *pb.ReadKeyUsageWithMacReply, err error)
	ReadKeyUsageBulk(ctx context.Context, req *pb.ReadKeyUsageBulkRequest) (res *pb.ReadKeyUsageBulkReply, err error)
	ExpireUsageBulk(ctx context.Context, req *pb.ExpireUsageBulkRequest) (res *pb.ExpireUsageBulkReply, err error)
}

type basicUsageService struct{}

func (b *basicUsageService) CreateKeyUsageInfo(ctx context.Context, req *pb.CreateKeyUsageInfoRequest) (res *pb.CreateKeyUsageInfoReply, err error) {
	fmt.Fprintf(os.Stdout, "CreateKeyUsageInfo: %v", req)
	do := KeyUsageObject{
		Key: req.Key,
		//Status: req.Status,
		Status: "standby",
	}

	id, err := do.Create()
	if err != nil {
		return
	}

	res = &pb.CreateKeyUsageInfoReply{
		Id: id,
	}
	return res, err
}

// NewBasicUsageService returns a naive, stateless implementation of UsageService.
func NewBasicUsageService() UsageService {
	return &basicUsageService{}
}

// New returns a UsageService with all of the expected middleware wired in.
func New(middleware []Middleware) UsageService {
	var svc UsageService = NewBasicUsageService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

func (b *basicUsageService) UpdateKeyUsageInfo(ctx context.Context, req *pb.UpdateKeyUsageInfoRequest) (res *pb.UpdateKeyUsageInfoReply, err error) {
	mac := []string{req.MacAddress}
	var user []string
	fmt.Fprintf(os.Stdout, "userId: %v\n", req.UserId)
	if len(req.UserId) > 0 {
		user = append(user, req.UserId)
	}
	do := KeyUsageObject{
		Key:        req.Key,
		MacAddress: mac,
		Users:      user,
	}

	cnt, err := do.UpdateKeyUsage()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return
	}

	res = &pb.UpdateKeyUsageInfoReply{
		UpdatedCount: cnt,
	}
	return res, err
}

func (b *basicUsageService) ReadKeyUsageInfo(ctx context.Context, req *pb.ReadKeyUsageInfoRequest) (res *pb.ReadKeyUsageInfoReply, err error) {
	do := KeyUsageObject{
		Key: req.Key,
	}

	usage, err := do.ReadKeyUsage()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return
	}

	res = &pb.ReadKeyUsageInfoReply{
		Id:         usage.Id.Hex(),
		Key:        usage.Key,
		MacAddress: usage.MacAddress,
		UserId:     usage.Users,
		ActiveTime: usage.Time.ActivationTime.Format(config.GetDefaultTimeFormat()),
		Status:     usage.Status,
	}

	return res, err
}

func (b *basicUsageService) KeepAliveUsage(ctx context.Context, req *pb.KeepAliveUsageRequest) (res *pb.KeepAliveUsageReply, err error) {
	res = &pb.KeepAliveUsageReply{
		Pong: req.Ping,
	}

	return res, err
}

func (b *basicUsageService) ReadKeyUsageWithMac(ctx context.Context, req *pb.ReadKeyUsageWithMacRequest) (res *pb.ReadKeyUsageWithMacReply, err error) {
	do := KeyUsageObject{
		MacAddress: req.MacAddress,
	}
	usageList, _ := do.ReadKeyWithMac()

	var result []*pb.ReadKeyUsageWithMacReplyUsage
	for _, usage := range usageList {
		result = append(result, &pb.ReadKeyUsageWithMacReplyUsage{
			Id:         usage.Id.Hex(),
			Key:        usage.Key,
			MacAddress: usage.MacAddress,
			UserId:     usage.Users,
			ActiveTime: usage.Time.ActivationTime.Format(config.GetDefaultTimeFormat()),
			Status:     usage.Status,
		})
	}

	res = &pb.ReadKeyUsageWithMacReply{
		KeyUsages: result,
		Count:     int64(len(result)),
	}

	return res, err
}

func (b *basicUsageService) ReadKeyUsageBulk(ctx context.Context, req *pb.ReadKeyUsageBulkRequest) (res *pb.ReadKeyUsageBulkReply, err error) {
	do := KeyUsageObject{}
	usageList, err := do.ReadUsagesBulkWithKey(req.KeyList)
	if err != nil {
		return
	}

	var result []*pb.ReadKeyUsageBulkReplyUsage
	for _, usage := range usageList {
		result = append(result, &pb.ReadKeyUsageBulkReplyUsage{
			Id:         usage.Id.Hex(),
			Key:        usage.Key,
			MacAddress: usage.MacAddress,
			UserId:     usage.Users,
			ActiveTime: usage.Time.ActivationTime.Format(config.GetDefaultTimeFormat()),
			Status:     usage.Status,
		})
	}

	res = &pb.ReadKeyUsageBulkReply{
		KeyUsages: result,
		Count:     int64(len(result)),
	}

	return res, err
}

func (b *basicUsageService) ExpireUsageBulk(ctx context.Context, req *pb.ExpireUsageBulkRequest) (res *pb.ExpireUsageBulkReply, err error) {
	if len(req.IdList) == 0 {
		err = errors.New("usage: id_list is mandatory")
		return
	}

	var obIdLIst []primitive.ObjectID
	for _, id := range req.IdList {
		objectId, _ := primitive.ObjectIDFromHex(id)
		obIdLIst = append(obIdLIst, objectId)
	}

	do := KeyUsageObject{}
	cnt, err := do.UpdateUsagesToExpired(obIdLIst)
	if err != nil {
		return
	}

	res = &pb.ExpireUsageBulkReply{
		Count: cnt,
	}

	return res, err
}
