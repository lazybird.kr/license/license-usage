package grpc

import (
	"context"
	"license-usage/usage/pkg/endpoint"
	"license-usage/usage/pkg/grpc/pb"

	"github.com/go-kit/kit/transport/grpc"
	context1 "golang.org/x/net/context"
)

func makeCreateKeyUsageInfoHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.CreateKeyUsageInfoEndpoint, decodeCreateKeyUsageInfoRequest, encodeCreateKeyUsageInfoResponse, options...)
}

func decodeCreateKeyUsageInfoRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(*pb.CreateKeyUsageInfoRequest)
	return endpoint.CreateKeyUsageInfoRequest{
		Req: req,
	}, nil
}

func encodeCreateKeyUsageInfoResponse(_ context.Context, r interface{}) (interface{}, error) {
	res := r.(endpoint.CreateKeyUsageInfoResponse)
	return res.Res, res.Failed()
}
func (g *grpcServer) CreateKeyUsageInfo(ctx context1.Context, req *pb.CreateKeyUsageInfoRequest) (*pb.CreateKeyUsageInfoReply, error) {
	_, rep, err := g.createKeyUsageInfo.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.CreateKeyUsageInfoReply), nil
}

func makeUpdateKeyUsageInfoHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.UpdateKeyUsageInfoEndpoint, decodeUpdateKeyUsageInfoRequest, encodeUpdateKeyUsageInfoResponse, options...)
}

func decodeUpdateKeyUsageInfoRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(*pb.UpdateKeyUsageInfoRequest)
	return endpoint.UpdateKeyUsageInfoRequest{
		Req: req,
	}, nil
}

func encodeUpdateKeyUsageInfoResponse(_ context.Context, r interface{}) (interface{}, error) {
	res := r.(endpoint.UpdateKeyUsageInfoResponse)
	return res.Res, res.Failed()
}
func (g *grpcServer) UpdateKeyUsageInfo(ctx context1.Context, req *pb.UpdateKeyUsageInfoRequest) (*pb.UpdateKeyUsageInfoReply, error) {
	_, rep, err := g.updateKeyUsageInfo.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.UpdateKeyUsageInfoReply), nil
}

func makeReadKeyUsageInfoHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.ReadKeyUsageInfoEndpoint, decodeReadKeyUsageInfoRequest, encodeReadKeyUsageInfoResponse, options...)
}

func decodeReadKeyUsageInfoRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(*pb.ReadKeyUsageInfoRequest)
	return endpoint.ReadKeyUsageInfoRequest{
		Req: req,
	}, nil
}

func encodeReadKeyUsageInfoResponse(_ context.Context, r interface{}) (interface{}, error) {
	res := r.(endpoint.ReadKeyUsageInfoResponse)
	return res.Res, res.Failed()
}
func (g *grpcServer) ReadKeyUsageInfo(ctx context1.Context, req *pb.ReadKeyUsageInfoRequest) (*pb.ReadKeyUsageInfoReply, error) {
	_, rep, err := g.readKeyUsageInfo.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.ReadKeyUsageInfoReply), nil
}

func makeKeepAliveUsageHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.KeepAliveUsageEndpoint, decodeKeepAliveUsageRequest, encodeKeepAliveUsageResponse, options...)
}

func decodeKeepAliveUsageRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(*pb.KeepAliveUsageRequest)
	return endpoint.KeepAliveUsageRequest{
		Req: req,
	}, nil
}

func encodeKeepAliveUsageResponse(_ context.Context, r interface{}) (interface{}, error) {
	res := r.(endpoint.KeepAliveUsageResponse)
	return res.Res, res.Failed()
}
func (g *grpcServer) KeepAliveUsage(ctx context1.Context, req *pb.KeepAliveUsageRequest) (*pb.KeepAliveUsageReply, error) {
	_, rep, err := g.keepAliveUsage.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.KeepAliveUsageReply), nil
}

func makeReadKeyUsageWithMacHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.ReadKeyUsageWithMacEndpoint, decodeReadKeyUsageWithMacRequest, encodeReadKeyUsageWithMacResponse, options...)
}

func decodeReadKeyUsageWithMacRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(*pb.ReadKeyUsageWithMacRequest)
	return endpoint.ReadKeyUsageWithMacRequest{
		Req: req,
	}, nil
}

func encodeReadKeyUsageWithMacResponse(_ context.Context, r interface{}) (interface{}, error) {
	res := r.(endpoint.ReadKeyUsageWithMacResponse)
	return res.Res, res.Failed()
}
func (g *grpcServer) ReadKeyUsageWithMac(ctx context1.Context, req *pb.ReadKeyUsageWithMacRequest) (*pb.ReadKeyUsageWithMacReply, error) {
	_, rep, err := g.readKeyUsageWithMac.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.ReadKeyUsageWithMacReply), nil
}

func makeReadKeyUsageBulkHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.ReadKeyUsageBulkEndpoint, decodeReadKeyUsageBulkRequest, encodeReadKeyUsageBulkResponse, options...)
}

func decodeReadKeyUsageBulkRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(*pb.ReadKeyUsageBulkRequest)
	return endpoint.ReadKeyUsageBulkRequest{
		Req: req,
	}, nil
}

func encodeReadKeyUsageBulkResponse(_ context.Context, r interface{}) (interface{}, error) {
	res := r.(endpoint.ReadKeyUsageBulkResponse)
	return res.Res, res.Failed()
}
func (g *grpcServer) ReadKeyUsageBulk(ctx context1.Context, req *pb.ReadKeyUsageBulkRequest) (*pb.ReadKeyUsageBulkReply, error) {
	_, rep, err := g.readKeyUsageBulk.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.ReadKeyUsageBulkReply), nil
}

func makeExpireUsageBulkHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.ExpireUsageBulkEndpoint, decodeExpireUsageBulkRequest, encodeExpireUsageBulkResponse, options...)
}

func decodeExpireUsageBulkRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(*pb.ExpireUsageBulkRequest)
	return endpoint.ExpireUsageBulkRequest{
		Req: req,
	}, nil
}

func encodeExpireUsageBulkResponse(_ context.Context, r interface{}) (interface{}, error) {
	res := r.(endpoint.ExpireUsageBulkResponse)
	return res.Res, res.Failed()
}
func (g *grpcServer) ExpireUsageBulk(ctx context1.Context, req *pb.ExpireUsageBulkRequest) (*pb.ExpireUsageBulkReply, error) {
	_, rep, err := g.expireUsageBulk.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.ExpireUsageBulkReply), nil
}
