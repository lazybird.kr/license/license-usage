package endpoint

import (
	"context"
	"license-usage/usage/pkg/grpc/pb"
	service "license-usage/usage/pkg/service"

	endpoint "github.com/go-kit/kit/endpoint"
)

// CreateKeyUsageInfoRequest collects the request parameters for the CreateKeyUsageInfo method.
type CreateKeyUsageInfoRequest struct {
	Req *pb.CreateKeyUsageInfoRequest `json:"req"`
}

// CreateKeyUsageInfoResponse collects the response parameters for the CreateKeyUsageInfo method.
type CreateKeyUsageInfoResponse struct {
	Res *pb.CreateKeyUsageInfoReply `json:"res"`
	Err error                       `json:"err"`
}

// MakeCreateKeyUsageInfoEndpoint returns an endpoint that invokes CreateKeyUsageInfo on the service.
func MakeCreateKeyUsageInfoEndpoint(s service.UsageService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateKeyUsageInfoRequest)
		res, err := s.CreateKeyUsageInfo(ctx, req.Req)
		return CreateKeyUsageInfoResponse{
			Err: err,
			Res: res,
		}, nil
	}
}

// Failed implements Failer.
func (r CreateKeyUsageInfoResponse) Failed() error {
	return r.Err
}

// Failure is an interface that should be implemented by response types.
// Response encoders can check if responses are Failer, and if so they've
// failed, and if so encode them using a separate write path based on the error.
type Failure interface {
	Failed() error
}

// CreateKeyUsageInfo implements Service. Primarily useful in a client.
func (e Endpoints) CreateKeyUsageInfo(ctx context.Context, req *pb.CreateKeyUsageInfoRequest) (res *pb.CreateKeyUsageInfoReply, err error) {
	request := CreateKeyUsageInfoRequest{Req: req}
	response, err := e.CreateKeyUsageInfoEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(CreateKeyUsageInfoResponse).Res, response.(CreateKeyUsageInfoResponse).Err
}

// UpdateKeyUsageInfoRequest collects the request parameters for the UpdateKeyUsageInfo method.
type UpdateKeyUsageInfoRequest struct {
	Req *pb.UpdateKeyUsageInfoRequest `json:"req"`
}

// UpdateKeyUsageInfoResponse collects the response parameters for the UpdateKeyUsageInfo method.
type UpdateKeyUsageInfoResponse struct {
	Res *pb.UpdateKeyUsageInfoReply `json:"res"`
	Err error                       `json:"err"`
}

// Failed implements Failer.
func (r UpdateKeyUsageInfoResponse) Failed() error {
	return r.Err
}

// MakeUpdateKeyUsageInfoEndpoint returns an endpoint that invokes UpdateKeyUsageInfo on the service.
func MakeUpdateKeyUsageInfoEndpoint(s service.UsageService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(UpdateKeyUsageInfoRequest)
		res, err := s.UpdateKeyUsageInfo(ctx, req.Req)
		return UpdateKeyUsageInfoResponse{
			Err: err,
			Res: res,
		}, nil
	}
}

// UpdateKeyUsageInfo implements Service. Primarily useful in a client.
func (e Endpoints) UpdateKeyUsageInfo(ctx context.Context, req *pb.UpdateKeyUsageInfoRequest) (res *pb.UpdateKeyUsageInfoReply, err error) {
	request := UpdateKeyUsageInfoRequest{Req: req}
	response, err := e.UpdateKeyUsageInfoEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(UpdateKeyUsageInfoResponse).Res, response.(UpdateKeyUsageInfoResponse).Err
}

// ReadKeyUsageInfoRequest collects the request parameters for the ReadKeyUsageInfo method.
type ReadKeyUsageInfoRequest struct {
	Req *pb.ReadKeyUsageInfoRequest `json:"req"`
}

// ReadKeyUsageInfoResponse collects the response parameters for the ReadKeyUsageInfo method.
type ReadKeyUsageInfoResponse struct {
	Res *pb.ReadKeyUsageInfoReply `json:"res"`
	Err error                     `json:"err"`
}

// MakeReadKeyUsageInfoEndpoint returns an endpoint that invokes ReadKeyUsageInfo on the service.
func MakeReadKeyUsageInfoEndpoint(s service.UsageService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(ReadKeyUsageInfoRequest)
		res, err := s.ReadKeyUsageInfo(ctx, req.Req)
		return ReadKeyUsageInfoResponse{
			Err: err,
			Res: res,
		}, nil
	}
}

// Failed implements Failer.
func (r ReadKeyUsageInfoResponse) Failed() error {
	return r.Err
}

// ReadKeyUsageInfo implements Service. Primarily useful in a client.
func (e Endpoints) ReadKeyUsageInfo(ctx context.Context, req *pb.ReadKeyUsageInfoRequest) (res *pb.ReadKeyUsageInfoReply, err error) {
	request := ReadKeyUsageInfoRequest{Req: req}
	response, err := e.ReadKeyUsageInfoEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(ReadKeyUsageInfoResponse).Res, response.(ReadKeyUsageInfoResponse).Err
}

// KeepAliveUsageRequest collects the request parameters for the KeepAliveUsage method.
type KeepAliveUsageRequest struct {
	Req *pb.KeepAliveUsageRequest `json:"req"`
}

// KeepAliveUsageResponse collects the response parameters for the KeepAliveUsage method.
type KeepAliveUsageResponse struct {
	Res *pb.KeepAliveUsageReply `json:"res"`
	Err error                   `json:"err"`
}

// MakeKeepAliveUsageEndpoint returns an endpoint that invokes KeepAliveUsage on the service.
func MakeKeepAliveUsageEndpoint(s service.UsageService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(KeepAliveUsageRequest)
		res, err := s.KeepAliveUsage(ctx, req.Req)
		return KeepAliveUsageResponse{
			Err: err,
			Res: res,
		}, nil
	}
}

// Failed implements Failer.
func (r KeepAliveUsageResponse) Failed() error {
	return r.Err
}

// KeepAliveUsage implements Service. Primarily useful in a client.
func (e Endpoints) KeepAliveUsage(ctx context.Context, req *pb.KeepAliveUsageRequest) (res *pb.KeepAliveUsageReply, err error) {
	request := KeepAliveUsageRequest{Req: req}
	response, err := e.KeepAliveUsageEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(KeepAliveUsageResponse).Res, response.(KeepAliveUsageResponse).Err
}

// ReadKeyUsageWithMacRequest collects the request parameters for the ReadKeyUsageWithMac method.
type ReadKeyUsageWithMacRequest struct {
	Req *pb.ReadKeyUsageWithMacRequest `json:"req"`
}

// ReadKeyUsageWithMacResponse collects the response parameters for the ReadKeyUsageWithMac method.
type ReadKeyUsageWithMacResponse struct {
	Res *pb.ReadKeyUsageWithMacReply `json:"res"`
	Err error                        `json:"err"`
}

// MakeReadKeyUsageWithMacEndpoint returns an endpoint that invokes ReadKeyUsageWithMac on the service.
func MakeReadKeyUsageWithMacEndpoint(s service.UsageService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(ReadKeyUsageWithMacRequest)
		res, err := s.ReadKeyUsageWithMac(ctx, req.Req)
		return ReadKeyUsageWithMacResponse{
			Err: err,
			Res: res,
		}, nil
	}
}

// Failed implements Failer.
func (r ReadKeyUsageWithMacResponse) Failed() error {
	return r.Err
}

// ReadKeyUsageWithMac implements Service. Primarily useful in a client.
func (e Endpoints) ReadKeyUsageWithMac(ctx context.Context, req *pb.ReadKeyUsageWithMacRequest) (res *pb.ReadKeyUsageWithMacReply, err error) {
	request := ReadKeyUsageWithMacRequest{Req: req}
	response, err := e.ReadKeyUsageWithMacEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(ReadKeyUsageWithMacResponse).Res, response.(ReadKeyUsageWithMacResponse).Err
}

// ReadKeyUsageBulkRequest collects the request parameters for the ReadKeyUsageBulk method.
type ReadKeyUsageBulkRequest struct {
	Req *pb.ReadKeyUsageBulkRequest `json:"req"`
}

// ReadKeyUsageBulkResponse collects the response parameters for the ReadKeyUsageBulk method.
type ReadKeyUsageBulkResponse struct {
	Res *pb.ReadKeyUsageBulkReply `json:"res"`
	Err error                     `json:"err"`
}

// MakeReadKeyUsageBulkEndpoint returns an endpoint that invokes ReadKeyUsageBulk on the service.
func MakeReadKeyUsageBulkEndpoint(s service.UsageService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(ReadKeyUsageBulkRequest)
		res, err := s.ReadKeyUsageBulk(ctx, req.Req)
		return ReadKeyUsageBulkResponse{
			Err: err,
			Res: res,
		}, nil
	}
}

// Failed implements Failer.
func (r ReadKeyUsageBulkResponse) Failed() error {
	return r.Err
}

// ReadKeyUsageBulk implements Service. Primarily useful in a client.
func (e Endpoints) ReadKeyUsageBulk(ctx context.Context, req *pb.ReadKeyUsageBulkRequest) (res *pb.ReadKeyUsageBulkReply, err error) {
	request := ReadKeyUsageBulkRequest{Req: req}
	response, err := e.ReadKeyUsageBulkEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(ReadKeyUsageBulkResponse).Res, response.(ReadKeyUsageBulkResponse).Err
}

// ExpireUsageBulkRequest collects the request parameters for the ExpireUsageBulk method.
type ExpireUsageBulkRequest struct {
	Req *pb.ExpireUsageBulkRequest `json:"req"`
}

// ExpireUsageBulkResponse collects the response parameters for the ExpireUsageBulk method.
type ExpireUsageBulkResponse struct {
	Res *pb.ExpireUsageBulkReply `json:"res"`
	Err error                    `json:"err"`
}

// MakeExpireUsageBulkEndpoint returns an endpoint that invokes ExpireUsageBulk on the service.
func MakeExpireUsageBulkEndpoint(s service.UsageService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(ExpireUsageBulkRequest)
		res, err := s.ExpireUsageBulk(ctx, req.Req)
		return ExpireUsageBulkResponse{
			Err: err,
			Res: res,
		}, nil
	}
}

// Failed implements Failer.
func (r ExpireUsageBulkResponse) Failed() error {
	return r.Err
}

// ExpireUsageBulk implements Service. Primarily useful in a client.
func (e Endpoints) ExpireUsageBulk(ctx context.Context, req *pb.ExpireUsageBulkRequest) (res *pb.ExpireUsageBulkReply, err error) {
	request := ExpireUsageBulkRequest{Req: req}
	response, err := e.ExpireUsageBulkEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(ExpireUsageBulkResponse).Res, response.(ExpireUsageBulkResponse).Err
}
