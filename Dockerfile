FROM golang:latest as build
ENV LAZYBIRD_LICENSE /go/src
ENV GO111MODULE=on
ENV CGO_ENABLED=0

WORKDIR /go/src
RUN git clone https://gitlab.com/lazybird.kr/license/license-common.git
WORKDIR /go/src/license-usage
COPY go.mod .
COPY go.sum .
RUN go mod download

ADD . .
RUN go build -o app_license_usage license-usage/usage/cmd

FROM alpine:3.9
ENV LAZYBIRD_LICENSE /var/local
WORKDIR /var/local/license-common
COPY --from=build /go/src/license-common/docker_config.json ./config.json
WORKDIR /usr/local/bin
COPY --from=build /go/src/license-usage/app_license_usage /usr/local/bin/app_license_usage

ENTRYPOINT ["app_license_usage"]